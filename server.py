from videoplayer import VideoPlayer
from gevent.pywsgi import WSGIServer
from flask import Flask, render_template, redirect, request, session, Response, url_for, current_app, make_response
import requests

class FlaskWrapper:

	def __init__(self, videopath):
		self.app = Flask(__name__)
		self.videopath = videopath
		self.videoplayer = VideoPlayer(self.videopath)
		self.server = None

	def start_playback(self):
		print("Got command, starting playback")
		self.videoplayer.shell_playback_no_wait()
		return "ok"

	def add_endpoint(self):
		self.app.add_url_rule('/start_playback', 'start_playback', self.start_playback, methods=['GET'])

	def run(self):
		self.add_endpoint()
		self.server = WSGIServer(5000, self.app)
		self.server.serve_forever()

if __name__ == "__main__":
	server = FlaskWrapper("/home/mad/Videos/sample.mp4")
	server.run()
	print("Server started")