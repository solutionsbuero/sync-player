import vlc
import subprocess

class VideoPlayer:

	def __init__(self, videopath):
		self.videopath = videopath
		self.player = vlc.MediaPlayer(self.videopath)

	def start_playback(self):
		self.player.play()

	def shell_playback(self):
		player_process = subprocess.Popen("cvlc -f " + self.videopath + " vlc://quit", shell=True)
		player_process.wait()

	def shell_playback_no_wait(self):
		subprocess.Popen("cvlc -f " + self.videopath + " vlc://quit", shell=True)

if __name__ == "__main__":
	player = VideoPlayer("/home/dinu/Videos/sample.mp4")
	player.shell_playback()
